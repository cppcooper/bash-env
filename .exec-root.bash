#!/usr/bin/bash
exec_root_DIR="$(dirname "$(readlink -f "${BASH_SOURCE[0]}")")"
echo    " -New Bash Session-"
echo -e "   \e[43m\e[30m[$(date +%a) $(date +%H:%M:%S)]\033[0m"
source $exec_root_DIR/.bash-core-env.bash
source $exec_root_DIR/.bash-core-shell.bash
echo "Shell profile loaded."
echo ""
lsblk

