#!/usr/bin/bash
source $bash_env_root/functions/scripting.bash
sourcedir $bash_env_root/config
sourcedir $bash_env_root/functions
export PATH="$bash_env_root/commands:$bash_env_root/shell-scripts:$bash_env_root/bin:$PATH"
echo
echo "Environment loaded."
