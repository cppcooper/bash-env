#!/usr/bin/bash
bash_env_root="$(dirname "$(readlink -f "${BASH_SOURCE[0]}")")"
echo    " -New Bash Session-"
echo -e "   \e[43m\e[30m[$(date +%a) $(date +%H:%M:%S)]\033[0m"
source $bash_env_root/.bash-core-env.bash
source $bash_env_root/.bash-core-shell.bash
if windows
then
:
else
    uPATH="$uPATH:/opt/maple2017/bin"
    uPATH="$uPATH:$HOME/bin"
    uPATH="$uPATH:$HOME/scripts"
fi
export PATH="$uPATH:$PATH"
export EDITOR=nano
export MAKEFLAGS="-j8"
export QT_SCREEN_SCALE_FACTORS="1;1"
echo "Shell profile loaded."
echo ""
daylight -l 49.09

WDIR=$(pwd)
if [[ $WDIR = "/" ]]; then
    if ! haspriv; then
        df -h
        su
    fi
elif [[ $WDIR =~ "project" ]]; then
    ls -Aw50
else
    ls
fi
