#!/usr/bin/bash
source $bash_env_root/functions/scripting.bash
source $bash_env_root/functions/ssh.bash

if ! windows; then
  lock="/tmp/.lock-sshbash"
  exec 3>$lock
  flock --timeout 300 3 || exit 1
fi

debug_ "debug mode on"
if [ -z "$CYGWIN_SHELL" ] && [ -z "$MSYS_SHELL" ]
then
  sshcount=0
  # To ensure we connect across ttys and even if we didn't run it at the tty level
  # we need to check a number of things. (and that it works on windows)
  # Check:
  #   that there is at least one agent running
  #   that the running agent is referenced inside agent.sh
  # If it is not the agent referenced inside agent.sh the user may have multiple running
  for pid in $(get-agents | get-pid)
  do
    ((sshcount++))
  done
  debug_ "ssh-agent count: $sshcount"
  if ((sshcount == 0))
  then
    debug_ "user has no ssh-agents open"
    start-ssh-agent --keep
                   #--keep micro-optimization
  else
    debug_ "attempting to connect to the ssh-agent referenced inside agent.sh"
    connect-ssh-agent 1> /dev/null
                   #  we want to display the connection info at the end, so we save it
    if ssh-add -l 2>&1 | grep "Error" &> /dev/null
    then
      debug_ "ssh agent not connected or started"
      start-ssh-agent 1> /dev/null
    elif windows
    then
      debug_ "Okay, we are connected to an agent but let's check that it is the PID inside agent.sh"
      PID=0
      for pid in $(get-agents | get-pid)
      do
        if [[ $pid == $SSH_AGENT_WINPID ]]
        then
          debug_ "PID from agent.sh is running"
          PID=$pid
          break
        fi
      done
      if ((PID == 0))
      then
        debug_ "Could not find PID from agent.sh"
        start-ssh-agent --keep 1> /dev/null
      fi
    fi
    print-ssh-connection-info
  fi
fi
if ! windows; then
  rm -rf $lock
  flock -u 3
fi
#unset -f get-agents
#unset -f get-pid
debug_ ssh.bash ends
