#!/usr/bin/bash
FUNCDIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )"
source $FUNCDIR/scripting.bash

function _umask_hook {
  # Record the default umask value on the 1st run
  [[ -z $DEFAULT_UMASK ]] && export DEFAULT_UMASK="$(builtin umask)"

  if [[ -n $UMASK ]]; then
    umask "$UMASK"
  else
    umask "$DEFAULT_UMASK"
  fi
}
